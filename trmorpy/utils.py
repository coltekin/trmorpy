#!/usr/bin/env python3

circtbl = str.maketrans({'â': 'a',
                         'û': 'u',
                         'î': 'i',
                         'Â': 'A',
                         'Î': 'İ',
                         'Û': 'U'})

tblupper = str.maketrans({'i': 'İ', 'ı': 'I'})
tbllower = str.maketrans({'İ': 'i', 'I': 'ı'})

def tr_upper(s):
    return s.translate(tblupper).upper()

def tr_lower(s):
    return s.translate(tbllower).lower()

def tr_capitalize(s):
    return tr_upper(s[0]) + tr_lower(s[1:])

def tr_title(s):
    words = s.split()
    for i, w in enumerate(words):
        words[i] = tr_capitalize(w)
    return " ".join(words)

def del_circumflex(s):
    return (s.translate(circtbl))

if __name__ == "main":
    pass
