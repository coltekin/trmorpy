#!/usr/bin/env python3
""" Simple ranker for words without context.

We intend to estimate the joint probability of a root (r) and
rest of the analysis string (a) using 'p(a) * p(r|a)'. 
    p(a)    - probability of an analysis string (without root) in
              the training data 
    p(r|a)  - given a particular analysis without root the
              probability of the root.

"""

import sys
import re
import json
from math import log

a_re = re.compile(r'(?P<r>[^⟨]+)(?P<a>⟨.*)')

class NoContextRanker():
    def __init__(self, ignore=None):
        if ignore is None:
            self.ignore = set(['⟨apos⟩', '⟨abbr⟩'])

    def train(self, files):
        """ Given an analysis string without root form, 
            we count occurrence of root forms with a.

            The results are stored as a dictionary of dictionaries
            Keys at the first level are the analysis strings without
            root which point to second level arrays whose keys (r) are
            the root forms. The values in the second level arrays are
            the frequencies (raw counts) of the r given a. We also
            keep total token counts at both levels and number of roots
            recorded in allsub-arrays for ease of computation during
            disambiguation.
        """
        self.stats, atokens = dict(), 0
        ratypes = 0 # this is r|a types, that is the same
                    # root is counted multiple times for each analysis it 
                    # participates. 
        for f in files:
            with open(f, 'rt') as inf:
                for line in inf:
                    line = line.strip()
                    if len(line) == 0: continue
                    w, ra = line.split('\t')
                    if ra is None or ra == '_' or '⟨X⟩' in ra:
                        continue
                    if self.ignore:
                        for sym in self.ignore:
                            ra = ra.replace(sym, '')
                    m = a_re.match(ra)
                    if not m: continue
                    r = m.group('r')
                    a = m.group('a')
                    if a not in self.stats:
                        self.stats[a] = {'##tokens': 0}
                    if r not in self.stats[a]:
                        ratypes = ratypes + 1
                    self.stats[a][r] = 1 + self.stats.get(r, 0)
                    self.stats[a]['##tokens'] += 1
                    atokens += 1

            self.stats["##tokens"] = atokens
            self.stats["##ratypes"] = ratypes
        
    def score(self, astring):
        """ We try to estimate the joint probability of the 
            root (r) and the analysis (a). We factor the joint 
            probability as, p(r, a) = p(r|a) p(a).
            the input model contains counts of r|a, and a. 
            For unobserved quantities, we use add-one smoothing.

                              tokenC(r|a) + 1
                    p(r|a) = ---------------------
                              tokenC(x|a) + typeC(x|a)

           for all roots x with analysis a. If both r and a are unknown,
           we estimate the probability from the number of times a new root
           was assigned to any analysis in the training data.

                               tokenC(a)
                    p(a) = ------------------------------
                             tokenC(words) + typeC(words)
        """

        m = re.match(a_re, astring)
        if not m:
            return float('-inf')
        r = m.group('r')
        a = m.group('a')
        ntokens = self.stats["##tokens"]
        ntypes = len(self.stats)

        if self.ignore:
            for sym in self.ignore:
                a = a.replace(sym, '')
        if a in self.stats:
            tokC_a = self.stats[a]['##tokens']
            typC_a = len(self.stats[a])

            if r in self.stats[a]:
                tokC_ra = self.stats[a][r]
            else:
                tokC_ra = 0
            p_a = (tokC_a + 1) / (ntokens + ntypes)
            p_ra = (tokC_ra + 1 ) / (tokC_a + typC_a)
            score = log(p_a) + log(p_ra)
        else:
            p_a = -log(ntokens + ntypes) # log(1 / (ntokens + ntypes))
            p_ra = log(self.stats['##ratypes'] / ntokens)
            score = p_a + p_ra
        return score

    def save(self, filename='m0.json'):
        with open(filename, 'wt') as outf:
            json.dump(self.stats, outf, ensure_ascii=False, indent=2)

    def load(self, filename='m0.json'):
        with open(filename, 'rt') as inf:
            self.stats = json.load(inf)

if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('inputs', nargs='+')
    ap.add_argument('--output', '-o', default='m0.json')
    args = ap.parse_args()

    rr = NoContextRanker()
    rr.train(args.inputs)
    rr.save(args.output)
