#!/usr/bin/env python3

import os
import sys
import hfst
import re
from importlib.resources import path
from trmorpy.tokenizer import tokenize
from trmorpy.trmorph2ud import trmorph_to_ud
from trmorpy.ranker0 import NoContextRanker
from trmorpy.utils import tr_upper, tr_lower, tr_title, del_circumflex
import trmorpy.data as data
#import pkgutil

ig_re = re.compile('(?P<root>.*?)'
      '(?P<pos>⟨[A-Z][A-Za-z0-9:]*⟩)'
      '(?P<inflections>.*?)'
      '(?P<rest>(⟨[^A-Z][A-Za-z0-9:]*⟩⟨[A-Z][A-Za-z0-9:]*⟩.*)|$)')

class TrMorph():
    def __init__(self, flexible_case=False, block_mredup=True, datadir=None):
        self.analyzer, self.generator = None, None
        self.generator_b = None # generatory that leaves boundary markes intact
        self.ranker0 = None # analysis scorer without context
        self.flexible_case = flexible_case
        self.block_mredup = flexible_case
        self.datadir = datadir
        self._load_analyzer()

    def _load_hfst(self, resname):
        if self.datadir is not None:
            istream = hfst.HfstInputStream(
                    os.path.join(self.datadir, resname))
        else:
            with path(data, resname) as fn:
                istream = hfst.HfstInputStream(str(fn))
        return istream.read()

    def _load_analyzer(self):
        self.analyzer = self._load_hfst('analyzer.hfst')

    def _load_generator(self):
        self.generator = self._load_hfst('generator.hfst')

    def _load_generator_boundary(self):
        self.generator_b = self._load_hfst('generator-b.hfst')

    def _load_ranker0(self):
        with path(data, 'm0.json') as fn:
            self.ranker0 = NoContextRanker()
            self.ranker0.load(fn)

    def analyze_word(self, word, best_only=False, rank=True):
#        if not self.analyzer:
#            self._load_analyzer()
        a = set(self.analyzer.lookup(word))
        if self.flexible_case:
            a |= set(self.analyzer.lookup(tr_title(word)))
            a |= set(self.analyzer.lookup(tr_lower(word)))
        if len(a) == 0:
            a = [('{}⟨X⟩'.format(word), 0)]
        if self.block_mredup and word[0] in {'m', 'M'}:
            # exclude ⟨mredup⟩ if the word has another analysis
            noredup = {x for x in a if '⟨mredup⟩' not in x[0]}
            if len(noredup):
                a = noredup
        if best_only:
            return self.rank0([x[0] for x in a], return_rank=False)[0]
        elif rank:
            return self.rank0([x[0] for x in a], return_rank=False)
        else:
            return [x[0] for x in a]

    def generate(self, analysis):
        if not self.generator:
            self._load_generator()
        g = set(self.generator.lookup(analysis))
        if len(g) == 0:
            g = [('??', 0)]
        return [x[0] for x in g]

    def tokenize(self, text, return_spaces=False, return_analyses=False):
        return tokenize(text, self.analyze_word,
                return_spaces=return_spaces, return_analyses=return_analyses)


    def disambiguate(self, analyses):
        #TODO: proper disambiguation
        return self.disambiguate0(analyses)

    def disambiguate0(self, analyses):
        return [self.rank0(a)[0][0] for a in analyses]

    def rank0(self, analyses, return_rank=True):
        if self.ranker0 is None:
            self._load_ranker0()
        scores = []
        for a in analyses:
            scores.append(self.ranker0.score(a))
        if return_rank:
            return sorted(zip(analyses, scores), key=lambda x: x[1],
                reverse=True)
        else:
            return [a for _, a in sorted(zip(scores,analyses), reverse=True)]

    def get_igs(self, form, analysis=None):
        if analysis is None:
            analysis = self.analyze_word(form, best_only=True)
        a_split = []
        m = ig_re.match(analysis)
        while m and m.group('root'):
            a_split.append((
                m.group('root'),
                m.group('pos').replace('⟨', '').replace('⟩', ''),
                [x.replace('⟨', '').replace('⟩', '') for x in\
                        m.group('inflections').split('⟩⟨')] \
                    if m.group('inflections') else []
            ))
            m = ig_re.match(m.group('rest'))

        ## Segment the surface form for proper forms/lemmas
        if self.generator_b is None:
            self._load_generator_boundary()
        match = False
        if '⟨X⟩' in analysis:
            segforms = [form]
        else:
            segforms = {s[0] for s in self.generator_b.lookup(analysis)}
            if self.flexible_case:
                w, a = analysis.split('⟨', 1)
                a = '⟨' + a
                segforms |= {s[0] for s in
                        self.generator_b.lookup(tr_upper(w)+a)}
                segforms |= {s[0] for s in
                        self.generator_b.lookup(tr_lower(w)+a)}
                segforms |= {s[0] for s in
                        self.generator_b.lookup(tr_title(w)+a)}
        for s in segforms:
            s_form = s.replace('⟪DB⟫', '').replace('⟪RB⟫', '')\
                      .replace('⟪MB⟫', '').replace('⟪IGB⟫', '')
            s_normform = tr_lower(del_circumflex(s_form))
            normform = tr_lower(del_circumflex(form))
            if s_normform == normform:
                match = True
                break
        s_split = (s.replace('⟪DB⟫', '').replace('⟪RB⟫', '').split('⟪IGB⟫'))
        assert(len(s_split) == len(a_split))

        def split_like(sp, s):
            """ Split s similar to the already split same-size string 'sp'
            """
            extra_chars = []
            spform = ''.join(sp)
            if s[0] in {'m', 'M'} and spform == s[1:]:
                extra_chars.append(0)
            for apos in "'’´′ʼ":
                if apos in s and s.replace(apos, '') == spform:
                    extra_chars.append(s.find("'") - len(extra_chars))
            #TODO: this is not complete
            if not extra_chars and len(spform) > len(s):
                s += " " * (len(spform) - len(s))
            i = 0
            ssp = []
            for seg in sp:
                start, stop = i, i + len(seg)
                for ext in extra_chars:
                    if ext in range(start, stop):
                        stop += 1
                ssp.append(s[start:stop])
                i = stop
            return ssp

        ig_forms = split_like([x.replace('⟪MB⟫', '') for x in s_split], form)

        igs = [] # tuples of <surface, lemma, pos, inflections>
        for i, (a, s) in enumerate(zip(a_split, s_split)):
            ig_lemma, ig_pos, ig_infl = a
            ig_surf = ig_forms[i]
            if len(s) == 0: # skip zero morphemes (copula)
                pass
            elif i > 0 and ig_lemma not in {'⟨ki⟩', '⟨cpl⟩', '⟨li⟩', '⟨lik⟩', '⟨siz⟩'}:
                prev_ig = igs.pop()
                ig_morphs = split_like(s.split('⟪MB⟫'), ig_surf)
                ig_lemma = prev_ig[0] + ig_morphs[0]
                if not ig_lemma.islower() and prev_ig[1].islower():
                    ig_lemma = tr_lower(ig_lemma)
                igs.append((prev_ig[0] + ig_surf, ig_lemma, ig_pos, ig_infl))
            else:
                if ig_lemma == '⟨ki⟩': #TODO: use -ki to distinguish from the free morpheme
                    ig_lemma = 'ki'
                elif ig_lemma == '⟨cpl⟩':
                    ig_lemma = 'i'
                elif ig_lemma == '⟨li⟩':
                    ig_lemma = 'li'
                elif ig_lemma == '⟨lik⟩':
                    ig_lemma = 'lik'
                elif ig_lemma == '⟨siz⟩':
                    ig_lemma = 'siz'
                igs.append((ig_surf, ig_lemma, ig_pos, ig_infl))
        return igs

    def ud_analysis(self, form, analysis=None):
        analyses = []
        for ig in self.get_igs(form, analysis):
            analyses.append(trmorph_to_ud(ig))
        return analyses

    def analyze(self, text, disambiguate=None, tokenize=True):
        """Analyze a given text. """

        if tokenize:
            tokens, analyses = self.tokenize(text, return_analyses=True)
        else:
            tokens = text.split()
            analyses = [[self.analyze_word(t) for t in tokens]]

        if disambiguate:
            return self.disambiguate(analyses)
        else:
            return analyses

    def lemmatize(self, text, disambiguate=None, tokenize=True):
        # TODO: for words wit multiple IG's we are currently ignoring
        # the non-first IG
        tokens, analyses = self.tokenize(text, return_analyses=True)
        lemmas = []
        for i, sent in enumerate(analyses):
            sa = []
            for j, tok in enumerate(sent):
                toka = set()
                for ta in tok:
                    igs = self.get_igs(tokens[i][j], ta)
                    toka.add(igs[0][1])
                sa.append(toka)
            lemmas.append(sa)
        return lemmas

if __name__ == '__main__':
    import numpy as np
    trm = TrMorph(flexible_case=True)
    for line in sys.stdin:
        w = line.strip()
        a = trm.analyze_word(w)
        print("\t", a)
        print("\t", trm.get_igs(w))
