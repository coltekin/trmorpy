from setuptools import setup

setup(name='trmorpy',
      version='0.1',
      description='Python 3 interface for TRmorph',
      url='http://github.com/coltekin/trmorpy',
      author='Çağrı Çöltekin',
      author_email='cagri@coltekin.net',
      license='MIT',
      packages=['trmorpy'],
      zip_safe=False,
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Topic :: Text Processing :: Linguistic',
      ],
      requires=[
          'hfst',
      ],
      include_package_data=True,
      package_data={'': ['data/*.hfst']},
)
